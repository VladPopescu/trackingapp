package ro.fils.trackingapp;

import android.app.Application;


import com.firebase.client.Firebase;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by vladp on 4/24/2016.
 */
public class MainApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
        Firebase.setAndroidContext(this);

    }
}



