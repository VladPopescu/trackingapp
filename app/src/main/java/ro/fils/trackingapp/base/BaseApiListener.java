package ro.fils.trackingapp.base;

public interface BaseApiListener {

	/**
	 * callback for api interface
	 * @param model
	 */
	public void onResponse(BaseModel model);

}