package ro.fils.trackingapp.base;

public class ModelFailureResponse extends BaseModel {
	private String description;

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}
}
