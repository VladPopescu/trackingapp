package ro.fils.trackingapp.utils;


import android.view.View;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.model.Achievement;
import ro.fils.trackingapp.model.User;
import ro.fils.trackingapp.model.modelGoal.DistanceGoal;
import ro.fils.trackingapp.model.LeaderboardEntry;
import ro.fils.trackingapp.model.LocationAddress;
import ro.fils.trackingapp.model.LocationMarked;
import ro.fils.trackingapp.model.LocationPoint;
import ro.fils.trackingapp.model.modelGoal.MarkerGoal;
import ro.fils.trackingapp.model.modelGoal.TimeGoal;

/**
 * Created by vladp on 6/29/2016.
 */
public class MainAppFragment extends BaseFragment {

    public static List<LocationPoint> userLocationsPoints = new ArrayList<>();
    public static List<LocationMarked> userLocationsMarked = new ArrayList<>();
    public static DistanceGoal userDistanceGoal = new DistanceGoal();
    public static TimeGoal userTimeGoal = new TimeGoal();
    public static MarkerGoal userMarkerGoal = new MarkerGoal();
    public static List<LeaderboardEntry> userLeaderboard = new ArrayList<>();
    public static List<Achievement> userAchievements = new ArrayList<>();
    public static User userLogged = new User();
    public static LeaderboardEntry leaderboardEntry = new LeaderboardEntry();


    @Override
    public void initUI(View view) {

    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }

    public void getUserLocations(String uid) {
        Firebase userReference = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/locations/");
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    LocationPoint locationPoint = new LocationPoint();
                    locationPoint.setLatitude((Double) postSnapshot.child("latitude").getValue());
                    locationPoint.setLongitude((Double) postSnapshot.child("longitude").getValue());
                    locationPoint.setDateTime((String) postSnapshot.child("dateTime").getValue());
                    locationPoint.setDateLocation((String) postSnapshot.child("dateLocation").getValue());
                    String adminArea = (String) postSnapshot.child("locationAddress").child("adminArea").getValue();
                    String countryName = (String) postSnapshot.child("locationAddress").child("countryName").getValue();
                    String locality = (String) postSnapshot.child("locationAddress").child("locality").getValue();
                    String subAdminArea = (String) postSnapshot.child("locationAddress").child("subAdminArea").getValue();
                    String subLocality = (String) postSnapshot.child("locationAddress").child("subLocality").getValue();
                    String subThroughfare = (String) postSnapshot.child("locationAddress").child("subThroughfare").getValue();
                    String throughfare = (String) postSnapshot.child("locationAddress").child("throughfare").getValue();
                    LocationAddress locationAddress = new LocationAddress(adminArea, countryName, locality, subAdminArea, subLocality, subThroughfare, throughfare);
                    locationPoint.setLocationAddress(locationAddress);
                    userLocationsPoints.add(locationPoint);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void getUserMarkers(String uid) {
        Firebase userReference = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/markedLocations/");
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    LocationMarked markedPoint = new LocationMarked();
                    markedPoint.setCoordinateName((String) postSnapshot.child("coordinateName").getValue());
                    markedPoint.setCoordinateDescription((String) postSnapshot.child("coordinateDescription").getValue());
                    markedPoint.setLocationType((String) postSnapshot.child("locationType").getValue());
                    markedPoint.setCoordinateLatitude((Double) postSnapshot.child("coordinateLatitude").getValue());
                    markedPoint.setCoordinateLongitude((Double) postSnapshot.child("coordinateLongitude").getValue());
                    userLocationsMarked.add(markedPoint);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void getUserGoals(String uid) {
        final Firebase userDistanceGoalsRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/goals/distanceGoals/");
        userDistanceGoalsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    userDistanceGoal.setName((String) postSnapshot.child("name").getValue());
                    userDistanceGoal.setLevel((Long) postSnapshot.child("level").getValue());
                    userDistanceGoal.setDistanceObjective((double) postSnapshot.child("distanceObjective").getValue());
                    userDistanceGoal.setKey(postSnapshot.getKey());
                    userDistanceGoal.setCategory((String) postSnapshot.child("category").getValue());
                    userDistanceGoal.setDateGoal((String) postSnapshot.child("dateGoal").getValue());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        Firebase userTimeGoalsRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/goals/timeGoals/");
        userTimeGoalsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    userTimeGoal.setName((String) postSnapshot.child("name").getValue());
                    userTimeGoal.setLevel((Long) postSnapshot.child("level").getValue());
                    userTimeGoal.setTimeObjective((double) postSnapshot.child("timeObjective").getValue());
                    userTimeGoal.setCategory((String) postSnapshot.child("category").getValue());
                    userTimeGoal.setDateGoal((String) postSnapshot.child("dateGoal").getValue());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        Firebase userMarkerGoalsRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/goals/markerGoals/");
        userMarkerGoalsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    userMarkerGoal.setName((String) postSnapshot.child("name").getValue());
                    userMarkerGoal.setLevel((Long) postSnapshot.child("level").getValue());
                    userMarkerGoal.setMarkerObjective((double) postSnapshot.child("markerObjective").getValue());
                    userMarkerGoal.setCategory((String) postSnapshot.child("category").getValue());
                    userMarkerGoal.setDateGoal((String) postSnapshot.child("dateGoal").getValue());
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    public void getUserAccountDetails(final String uid){
        Firebase userListRef = new Firebase("https://trackerappv.firebaseio.com/users/" + uid );


        userListRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userLogged.setEmail((String) dataSnapshot.child("email").getValue());
                userLogged.setId((String) dataSnapshot.child("id").getValue());
                userLogged.setPassword("");
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        Firebase userLeaderBoardRef = new Firebase("https://trackerappv.firebaseio.com/leaderboards/" + uid);

        userLeaderBoardRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                leaderboardEntry.setId(uid);
                leaderboardEntry.setScore((double) dataSnapshot.child("score").getValue());
                leaderboardEntry.setEmail((String) dataSnapshot.child("email").getValue());
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    public void getUserAchievements(String uid){
        Firebase userAchievementsRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/achievements/");
        userAchievementsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Achievement userAchievement = new Achievement();
                    userAchievement.setName((String) postSnapshot.child("name").getValue());
                    userAchievement.setLevel((Long) postSnapshot.child("level").getValue());
                    userAchievement.setObjective((Double) postSnapshot.child("objective").getValue());
                    userAchievement.setCategory((String) postSnapshot.child("category").getValue());
                    userAchievements.add(userAchievement);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    public void getLeaderBoards() {
        Firebase leaderboardsRef = new Firebase("https://trackerappv.firebaseio.com/leaderboards/");
        leaderboardsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    LeaderboardEntry entryLeaderboard = new LeaderboardEntry();
                    entryLeaderboard.setId(postSnapshot.getKey());
                    entryLeaderboard.setEmail((String) postSnapshot.child("email").getValue());
                    entryLeaderboard.setScore((double) postSnapshot.child("score").getValue());
                    userLeaderboard.add(entryLeaderboard);

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}
