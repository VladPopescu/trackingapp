package ro.fils.trackingapp.model;

/**
 * Created by vladp on 6/10/2016.
 */
public class LocationMarked {
    private double coordinateLatitude;
    private double coordinateLongitude;
    private String coordinateName;
    private String coordinateDescription;
    private String locationType;

    public LocationMarked(){

    }

    public LocationMarked(double coordinateLatitude, double coordinateLongitude, String coordinateName, String coordinateDescription, String locationType) {
        this.coordinateLatitude = coordinateLatitude;
        this.coordinateLongitude = coordinateLongitude;
        this.coordinateName = coordinateName;
        this.coordinateDescription = coordinateDescription;
        this.locationType = locationType;
    }

    public double getCoordinateLatitude() {
        return coordinateLatitude;
    }

    public void setCoordinateLatitude(double coordinateLatitude) {
        this.coordinateLatitude = coordinateLatitude;
    }

    public double getCoordinateLongitude() {
        return coordinateLongitude;
    }

    public void setCoordinateLongitude(double coordinateLongitude) {
        this.coordinateLongitude = coordinateLongitude;
    }

    public String getCoordinateName() {
        return coordinateName;
    }

    public void setCoordinateName(String coordinateName) {
        this.coordinateName = coordinateName;
    }

    public String getCoordinateDescription() {
        return coordinateDescription;
    }

    public void setCoordinateDescription(String coordinateDescription) {
        this.coordinateDescription = coordinateDescription;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }
}
