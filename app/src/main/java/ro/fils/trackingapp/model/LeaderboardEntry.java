package ro.fils.trackingapp.model;

/**
 * Created by vladp on 6/30/2016.
 */
public class LeaderboardEntry {
        private String id;
        private String email;
        private double score;

    public LeaderboardEntry(){

    }

    public LeaderboardEntry(String id, String email, double score) {
        this.id = id;
        this.email = email;
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
