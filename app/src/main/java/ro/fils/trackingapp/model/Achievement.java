package ro.fils.trackingapp.model;

/**
 * Created by vladp on 7/2/2016.
 */
public class Achievement {
    private String name;
    private String category;
    private Long  level;
    private double objective;

    public Achievement(){

    }

    public Achievement(String name, Long level, double objective, String category) {
        this.name = name;
        this.level = level;
        this.objective = objective;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public double getObjective() {
        return objective;
    }

    public void setObjective(double objective) {
        this.objective = objective;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
