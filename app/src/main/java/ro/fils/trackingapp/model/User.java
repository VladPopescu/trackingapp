package ro.fils.trackingapp.model;

import com.firebase.client.Firebase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import ro.fils.trackingapp.model.modelGoal.DistanceGoal;
import ro.fils.trackingapp.model.modelGoal.MarkerGoal;
import ro.fils.trackingapp.model.modelGoal.TimeGoal;

public class User {

    private String id;
    private String email;
    private String password;


    public User() {
    }

    public User(String id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void saveUser() {
        Firebase myFirebaseRef = new Firebase("https://trackerappv.firebaseio.com/");
        myFirebaseRef = myFirebaseRef.child("users").child(getId());
        myFirebaseRef.setValue(this);


        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = dateFormatter.format(Calendar.getInstance().getTime());

        long level = 1;
        Firebase userDistanceGoalRef = new Firebase("https://trackerappv.firebaseio.com/" + getId() + "/goals/distanceGoals");
        DistanceGoal goalDistance = new DistanceGoal("Distance Covered", level, 1000, "", "distance",date);
        userDistanceGoalRef.push().setValue(goalDistance);

        Firebase userTimeGoalRef = new Firebase("https://trackerappv.firebaseio.com/" + getId() + "/goals/timeGoals");
        TimeGoal goalTime = new TimeGoal("Time",level ,60 , "", "time", date);
        userTimeGoalRef.push().setValue(goalTime);

        double markerObjective = 5;
        Firebase userMarkerGoalRef = new Firebase("https://trackerappv.firebaseio.com/" + getId() + "/goals/markerGoals");
        MarkerGoal goalMarker = new MarkerGoal("Marked Locations", level, markerObjective, "", "marker", date);
        userMarkerGoalRef.push().setValue(goalMarker);

        double score = 0;
        Firebase leaderboardsRef = new Firebase("https://trackerappv.firebaseio.com/leaderboards");
        leaderboardsRef.child(getId()).child("score").setValue(score);
        leaderboardsRef.child(getId()).child("email").setValue(email);

    }

}