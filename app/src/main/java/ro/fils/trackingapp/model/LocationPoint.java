package ro.fils.trackingapp.model;

/**
 * Created by vladp on 3/21/2016.
 */
public class LocationPoint {

    private double latitude;
    private double longitude;
    private String dateTime;
    private String dateLocation;
    private LocationAddress locationAddress;

    public LocationPoint() {

    }

    public LocationPoint(double latitude, double longitude, String dateTime, String dateLocation, LocationAddress locationAddress ) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateTime = dateTime;
        this.dateLocation = dateLocation;
        this.locationAddress = locationAddress;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getDateLocation() {
        return dateLocation;
    }

    public void setDateLocation(String dateLocation) {
        this.dateLocation = dateLocation;
    }

    public LocationAddress getLocationAddress() {
        return locationAddress;
    }

    public void setLocationAddress(LocationAddress locationAddress) {
        this.locationAddress = locationAddress;
    }
}