package ro.fils.trackingapp.model.modelGoal;

/**
 * Created by vladp on 6/29/2016.
 */
public class DistanceGoal {
    private String name;
    private String key;
    private Long level;
    private String category;
    private double distanceObjective;
    private String dateGoal;

    public DistanceGoal(){

    }

    public DistanceGoal(String name, Long level, double distanceObjective, String key, String category, String dateGoal) {
        this.name = name;
        this.level = level;
        this.distanceObjective = distanceObjective;
        this.key = key;
        this.category = category;
        this.dateGoal = dateGoal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public double getDistanceObjective() {
        return distanceObjective;
    }

    public void setDistanceObjective(double distanceObjective) {
        this.distanceObjective = distanceObjective;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDateGoal() {
        return dateGoal;
    }

    public void setDateGoal(String dateGoal) {
        this.dateGoal = dateGoal;
    }
}
