package ro.fils.trackingapp.model.modelGoal;

/**
 * Created by vladp on 6/30/2016.
 */
public class MarkerGoal {
    private String name;
    private String key;
    private Long level;
    private String category;
    private double markerObjective;
    private String dateGoal;

    public MarkerGoal(){

    }

    public MarkerGoal(String name, Long level, double markerObjective, String key, String category, String dateGoal) {
        this.name = name;
        this.level = level;
        this.markerObjective = markerObjective;
        this.key = key;
        this.category = category;
        this.dateGoal = dateGoal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public double getMarkerObjective() {
        return markerObjective;
    }

    public void setMarkerObjective(double markerObjective) {
        this.markerObjective = markerObjective;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDateGoal() {
        return dateGoal;
    }

    public void setDateGoal(String dateGoal) {
        this.dateGoal = dateGoal;
    }
}
