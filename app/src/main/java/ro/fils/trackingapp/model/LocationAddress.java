package ro.fils.trackingapp.model;

/**
 * Created by vladp on 6/7/2016.
 */
public class LocationAddress {
    private String adminArea;
    private String countryName;
    private String locality;
    private String subAdminArea;
    private String subLocality;
    private String subThroughfare;
    private String  throughfare;


    public LocationAddress(String adminArea, String countryName, String locality, String subAdminArea, String subLocality, String subThroughfare, String throughfare) {
        this.adminArea = adminArea;
        this.countryName = countryName;
        this.locality = locality;
        this.subAdminArea = subAdminArea;
        this.subLocality = subLocality;
        this.subThroughfare = subThroughfare;
        this.throughfare = throughfare;
    }


    public String getAdminArea() {
        return adminArea;
    }

    public void setAdminArea(String adminArea) {
        this.adminArea = adminArea;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getSubAdminArea() {
        return subAdminArea;
    }

    public void setSubAdminArea(String subAdminArea) {
        this.subAdminArea = subAdminArea;
    }

    public String getSubLocality() {
        return subLocality;
    }

    public void setSubLocality(String subLocality) {
        this.subLocality = subLocality;
    }

    public String getSubThroughfare() {
        return subThroughfare;
    }

    public void setSubThroughfare(String subThroughfare) {
        this.subThroughfare = subThroughfare;
    }

    public String getThroughfare() {
        return throughfare;
    }

    public void setThroughfare(String throughfare) {
        this.throughfare = throughfare;
    }
}
