package ro.fils.trackingapp.model.modelGoal;

/**
 * Created by vladp on 6/29/2016.
 */
public class TimeGoal {
    private String name;
    private String key;
    private String category;
    private Long level;
    private double timeObjective;
    private String dateGoal;

    public TimeGoal() {

    }

    public TimeGoal(String name, Long level, double timeObjective, String key, String category, String dateGoal) {
        this.name = name;
        this.level = level;
        this.timeObjective = timeObjective;
        this.key = key;
        this.category = category;
        this.dateGoal = dateGoal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getLevel() {
        return level;
    }

    public void setLevel(Long level) {
        this.level = level;
    }

    public double getTimeObjective() {
        return timeObjective;
    }

    public void setTimeObjective(double timeObjective) {
        this.timeObjective = timeObjective;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDateGoal() {
        return dateGoal;
    }

    public void setDateGoal(String dateGoal) {
        this.dateGoal = dateGoal;
    }
}
