package ro.fils.trackingapp.adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.model.LeaderboardEntry;

public class LeaderboardAdapter extends ArrayAdapter<LeaderboardEntry> {

    public LeaderboardAdapter(Context context, ArrayList<LeaderboardEntry> leaderboardEntries) {
        super(context, 0, leaderboardEntries);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        LeaderboardEntry entryLeaderboard = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_layout, parent, false);
        }

        // Lookup view for data population
        TextView textViewEmail = (TextView) convertView.findViewById(R.id.userEmail);
        TextView textViewScore = (TextView) convertView.findViewById(R.id.userScore);
        TextView textViewNo = (TextView) convertView . findViewById(R.id.userNo);

        // Populate the data into the template view using the data object
        textViewEmail.setText(entryLeaderboard.getEmail());
        textViewEmail.setTextColor(Color.parseColor("#ffffff"));
        textViewEmail.setTypeface(null, Typeface.BOLD);

        textViewNo.setText(position + 1 + ". ");
        textViewNo.setTextColor(Color.parseColor("#ffffff"));
        textViewNo.setTypeface(null, Typeface.BOLD);

        textViewScore.setText("     Score: " + String.valueOf((int) entryLeaderboard.getScore()));
        textViewScore.setTextColor(Color.parseColor("#ffffff"));
        textViewScore.setTypeface(null, Typeface.BOLD);

        // Return the completed view to render on screen
        return convertView;
    }
}