package ro.fils.trackingapp.fragments.menuFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.firebase.client.Firebase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.adapter.LeaderboardAdapter;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.model.LeaderboardEntry;
import ro.fils.trackingapp.utils.MainAppFragment;

/**
 * Created by vladp on 6/29/2016.
 */
public class LeaderboardsFragment extends BaseFragment {
    ArrayList<LeaderboardEntry> userLeaderBoard = new ArrayList<>();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.leaderboards_fragment, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);

    }

    @Override
    public void initUI(View view) {


        userLeaderBoard = (ArrayList<LeaderboardEntry>) MainAppFragment.userLeaderboard;

        Collections.sort(userLeaderBoard, new Comparator<LeaderboardEntry>() {
            @Override
            public int compare(LeaderboardEntry c1, LeaderboardEntry c2) {
                return Double.compare(c1.getScore(), c2.getScore());
            }
        });
        Collections.reverse(userLeaderBoard);

        ListView list = (ListView) view.findViewById(R.id.leaderboards_listView);
        LeaderboardAdapter adapter = new LeaderboardAdapter(getContext(),userLeaderBoard);
        list.setAdapter(adapter);

    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }
}
