package ro.fils.trackingapp.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.fragments.mainFragments.MainFragmentActivity;
import ro.fils.trackingapp.model.User;

/**
 * Created by vladp on 6/18/2016.
 */
public class LoginFragment extends BaseFragment{

    private Button loginButton;
    private Firebase myFirebaseRef;
    private User user;
    private EditText editTextEmail;
    private EditText editTextPassword;

    public LoginFragment(){
    }

   LoginFragment(Bundle savedInstanceState) {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.login_fragment, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);

    }


    @Override
    public void initUI(View view) {
        myFirebaseRef =  new Firebase("https://trackerappv.firebaseio.com/");
        editTextEmail = (EditText) view.findViewById(R.id.editTextLoginEmail);
        editTextPassword = (EditText) view.findViewById(R.id.editTextLoginPassword);
        loginButton = (Button) view.findViewById(R.id.buttonLogin);
        myFirebaseRef.unauth();
        checkUserLogin();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = new User();
                user.setEmail(editTextEmail.getText().toString());
                user.setPassword(editTextPassword.getText().toString());


                myFirebaseRef.authWithPassword(
                        user.getEmail(),
                        user.getPassword(),
                        new Firebase.AuthResultHandler() {
                            @Override
                            public void onAuthenticated(AuthData authData) {
                                Intent intent = new Intent(getActivity().getApplicationContext(), MainFragmentActivity.class);
                                String uid = myFirebaseRef.getAuth().getUid();
                                intent.putExtra("user_id", uid);
                                startActivity(intent);
                            }

                            @Override
                            public void onAuthenticationError(FirebaseError firebaseError) {
                                Toast.makeText(getActivity().getApplicationContext(), "" + firebaseError.getMessage(), Toast.LENGTH_LONG).show();

                            }
                        }
                );
            }
        });
    }


    private void checkUserLogin() {
        if (myFirebaseRef.getAuth() != null) {
            Intent intent = new Intent(getActivity().getApplicationContext(), MainFragmentActivity.class);
            String uid = myFirebaseRef.getAuth().getUid();
            intent.putExtra("user_id", uid);
            startActivity(intent);
        }
    }


    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }
}
