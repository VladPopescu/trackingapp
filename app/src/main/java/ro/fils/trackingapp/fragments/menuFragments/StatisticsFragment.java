package ro.fils.trackingapp.fragments.menuFragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.firebase.client.Firebase;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;

/**
 * Created by vladp on 6/14/2016.
 */
public class StatisticsFragment extends BaseFragment implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener{
    private Button buttonWeekChart;
    private Button buttonLocationChart;
    private EditText editTextDateChart;
    private LinearLayout ll;
    private Context context;

    public StatisticsFragment(){
    }

    StatisticsFragment(Bundle savedInstanceState) {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Firebase.setAndroidContext(getContext());
        context = getActivity().getApplicationContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.statistics_fragment, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);

    }



    @Override
    public void initUI(View view) {
        ll = (LinearLayout) view.findViewById(R.id.layoutChart);
        buttonWeekChart = (Button) view.findViewById(R.id.buttonWeekChart);
        buttonLocationChart = (Button) view.findViewById(R.id.buttonLocationChart);
        editTextDateChart = (EditText) view.findViewById(R.id.editTextDateChart);

        buttonWeekChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.removeAllViewsInLayout();
                PieChart pieChart = new PieChart(getContext());
                pieChart.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
                ArrayList<Entry> entries = new ArrayList<>();
                entries.add(new Entry(4f, 0));
                entries.add(new Entry(8f, 1));

                PieDataSet dataset = new PieDataSet(entries, "");

                ArrayList<String> labels = new ArrayList<String>();
                labels.add("Indoor");
                labels.add("Outdoor");


                dataset.setColors(ColorTemplate.COLORFUL_COLORS);
                PieData data = new PieData(labels, dataset); // initialize Piedata
                pieChart.setData(data); //set data into chart
                pieChart.setHoleRadius(45f);
                pieChart.setDescription("");
                pieChart.animateXY(2000, 2000);
                ll.addView(pieChart);
            }
        });

        buttonLocationChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll.removeAllViewsInLayout();
            }
        });


    }

    public void onStart() {
        super.onStart();
//        editTextDateChart.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    Calendar now = Calendar.getInstance();
//                    DatePickerDialog dpd = DatePickerDialog.newInstance(
//                            StatisticsFragment.this,
//                            now.get(Calendar.YEAR),
//                            now.get(Calendar.MONTH),
//                            now.get(Calendar.DAY_OF_MONTH)
//                    );
//
//                    dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
//
//                }
//                editTextDateChart.clearFocus();
//
//            }
//        });

    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Date dateFormat = new Date(year - 1900, monthOfYear, dayOfMonth);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format(dateFormat);
        editTextDateChart.setText(date);
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {

    }
}
