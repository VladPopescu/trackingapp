package ro.fils.trackingapp.fragments.mainFragments;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.firebase.client.Firebase;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.fragments.menuFragments.HomePageFragment;
import ro.fils.trackingapp.fragments.LoginFragment;
import ro.fils.trackingapp.fragments.RegisterFragment;

/**
 * Created by vladp on 6/18/2016.
 */
public class MainFragment extends FragmentActivity{
    private LoginFragment fragmentLogin;
    private RegisterFragment fragmentRegister;
    private HomePageFragment fragmentHomePage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        launchHomePageView();
        Firebase.setAndroidContext(this);

    }

    public void launchLoginView(){
        if (fragmentLogin == null) {
            fragmentLogin = new LoginFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentLogin) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentLogin.isAdded()) {
                transaction.add(R.id.frame_container, fragmentLogin);
            } else {
                transaction.show(fragmentLogin);
            }

            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public void launchRegisterView(){
        if (fragmentRegister == null) {
            fragmentRegister = new RegisterFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentRegister) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentRegister.isAdded()) {
                transaction.add(R.id.frame_container, fragmentRegister);
            } else {
                transaction.show(fragmentRegister);
            }

            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public void launchHomePageView(){
        if (fragmentHomePage == null) {
            fragmentHomePage = new HomePageFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentHomePage) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentHomePage.isAdded()) {
                transaction.add(R.id.frame_container, fragmentHomePage);
            } else {
                transaction.show(fragmentHomePage);
            }

            transaction.addToBackStack(null);
            transaction.commit();
        }
    }


}
