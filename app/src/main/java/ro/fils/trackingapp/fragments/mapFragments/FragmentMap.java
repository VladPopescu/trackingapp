package ro.fils.trackingapp.fragments.mapFragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.fragments.mainFragments.MainFragmentActivity;
import ro.fils.trackingapp.model.LocationPoint;
import ro.fils.trackingapp.utils.MainAppFragment;

/**
 * Created by vladp on 4/24/2016.
 */
public class FragmentMap extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap mMap;
    private String path;
    private DateTime timeFrom;
    private DateTime timeTo;
    private ArrayList<LocationPoint> locationPoints = new ArrayList<>();
    private ArrayList<LocationPoint> locationPointsFiltered = new ArrayList<>();
    private ArrayList<LatLng> filteredPoints = new ArrayList<>();
    private TextView textViewDistance;
    private TextView textViewTimeInterval;
    private TextView textViewStartPoint;
    private TextView textViewFinalPoint;


    public FragmentMap(String path, DateTime timeFrom, DateTime timeTo) {
        this.path = path;
        this.timeFrom = timeFrom;
        this.timeTo = timeTo;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.map_fragment, container,
                false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
//        locationPoints = (ArrayListMainApp.points;<LocationPoint>)
        locationPoints = (ArrayList<LocationPoint>) MainAppFragment.userLocationsPoints;
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getActivity().getFragmentManager()
                    .findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }


    private void setUpMarkers(CameraPosition position) throws ParseException {

        List<MarkerOptions> markers = new ArrayList<>();
        int j = 0;
        for (LocationPoint p : locationPoints) {
            if (p.getDateLocation().equals(path)) {

                DateTime pTime = ((MainFragmentActivity) getActivity()).convertStringToDate(p.getDateTime());

                if ((timeFrom.getMinuteOfDay() <= pTime.getMinuteOfDay()) && (pTime.getMinuteOfDay() <= timeTo.getMinuteOfDay())) {


//                    filteredPoints.add(new LatLng(p.getLatitude(), p.getLongitude()));
                    MarkerOptions m = new MarkerOptions()
                            .position(new LatLng(p.getLatitude(), p.getLongitude()))
                            .title("Point " + j)
                            .snippet(p.getLocationAddress().getAdminArea() + " , " + p.getLocationAddress().getLocality() + " , " + p.getLocationAddress().getThroughfare())
                            .visible(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
                    markers.add(m);
                    mMap.addMarker(m);
                    j++;
                }
            }
        }


        for (int i = 0; i < markers.size() - 1; i++) {
            PolylineOptions polylineOptions = new PolylineOptions()
                    .add(markers.get(i).getPosition(), markers.get(i + 1).getPosition())
                    .visible(true);
            mMap.addPolyline(polylineOptions);
        }

    }

    private void setUpMap() {

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            @Override
            public void onCameraChange(CameraPosition position) {
                try {
                    setUpMarkers(position);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        });

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setBuildingsEnabled(false);
        mMap.setIndoorEnabled(true);


        LatLng startingPoint = new LatLng(locationPoints.get(0).getLatitude(), locationPoints.get(0).getLongitude());

        CameraUpdate upd = CameraUpdateFactory.newLatLngZoom(startingPoint,
                15);
        mMap.moveCamera(upd);
        // setUpMarkers();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setPadding(0, 0, 0, 10);
    }

    @Override
    public void onResume() {
        // mMap = null;
        super.onResume();
        setUpMapIfNeeded();
    }

    public void zoomOnEvent() {
        LatLng startingPoint = new LatLng(locationPoints.get(0).getLatitude(), locationPoints.get(0).getLongitude());

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(startingPoint) // Sets the center of the map
                .zoom(22) // Sets the zoom
                .bearing(304) // Sets the orientation of the camera to east
                .tilt(0) // Sets the tilt of the camera to 30 degrees
                .build(); // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
    }


    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onResponse(BaseModel model) {
        // TODO Auto-generated method stub

    }

    @Override
    public void initUI(View view) {
        locationPoints = (ArrayList<LocationPoint>) MainAppFragment.userLocationsPoints;
        System.out.println(MainAppFragment.userLocationsPoints.size());
        System.out.println(locationPoints.size());



        textViewDistance = (TextView) view.findViewById(R.id.textViewDistance);
        textViewStartPoint = (TextView) getView().findViewById(R.id.textViewStartPoint);
        textViewFinalPoint = (TextView) getView().findViewById(R.id.textViewFinalPoint);
        textViewTimeInterval = (TextView) view.findViewById(R.id.textViewTimeInterval);

        try {
            locationPointsFiltered = getFilteredRouteList((ArrayList<LocationPoint>) MainAppFragment.userLocationsPoints);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        computeRouteDistance(locationPointsFiltered);
        String timeFromInterval = timeFrom.getHourOfDay() + ":" + timeFrom.getMinuteOfHour();
        String timeToInterval = timeTo.getHourOfDay() + ":" + timeTo.getMinuteOfHour();
        textViewTimeInterval.setText("Time Interval: " + timeFromInterval + " - " + timeToInterval);
        textViewStartPoint.setText("Start:" + locationPointsFiltered.get(0).getLocationAddress().getSubLocality() + "," +locationPointsFiltered.get(0).getLocationAddress().getThroughfare() + " " + locationPointsFiltered.get(0).getLocationAddress().getSubThroughfare());
        textViewFinalPoint.setText("End:" + locationPointsFiltered.get(locationPointsFiltered.size() - 1).getLocationAddress().getSubLocality() + "," +locationPointsFiltered.get(locationPointsFiltered.size() - 1).getLocationAddress().getThroughfare() + " " + locationPointsFiltered.get(locationPointsFiltered.size() - 1).getLocationAddress().getSubThroughfare());
        textViewStartPoint.setTextSize(10);
        textViewFinalPoint.setTextSize(10);
    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public double getDistance(LatLng LatLng1, LatLng LatLng2) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(LatLng2.latitude);
        locationB.setLongitude(LatLng2.longitude);
        distance = locationA.distanceTo(locationB);
        return distance;

    }

    public ArrayList<LocationPoint> getFilteredRouteList(ArrayList<LocationPoint> locationPoints) throws ParseException {
        for (LocationPoint p : locationPoints) {
            if (p.getDateLocation().equals(path)) {

                DateTime pTime = ((MainFragmentActivity) getActivity()).convertStringToDate(p.getDateTime());
                if ((timeFrom.getMinuteOfDay() <= pTime.getMinuteOfDay()) && (pTime.getMinuteOfDay() <= timeTo.getMinuteOfDay())) {
                    locationPointsFiltered.add(p);
                }


            }
        }
        return locationPointsFiltered;
    }

    public void computeRouteDistance(ArrayList<LocationPoint> locationPointsFiltered){
        DecimalFormat df = new DecimalFormat("#.00");
        for(LocationPoint point : locationPointsFiltered){
            filteredPoints.add(new LatLng(point.getLatitude(), point.getLongitude()));
        }
        double totalDistance = 0.0;
        for (int i = 0; i < filteredPoints.size() - 1; i++){
            totalDistance = totalDistance + getDistance(filteredPoints.get(i), filteredPoints.get(i+1));
        }

        textViewDistance.setText("Distance: " + df.format(totalDistance) + " meters");

    }
}