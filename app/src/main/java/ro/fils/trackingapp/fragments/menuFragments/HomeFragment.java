package ro.fils.trackingapp.fragments.menuFragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.client.Firebase;


import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.fragments.mainFragments.MainFragment;
import ro.fils.trackingapp.fragments.mainFragments.MainFragmentActivity;
import ro.fils.trackingapp.model.LeaderboardEntry;
import ro.fils.trackingapp.utils.MainAppFragment;

/**
 * Created by vladp on 4/24/2016.
 */
public class HomeFragment extends BaseFragment {

    private Button buttonRequestLocation;
    private Button buttonMarkLocation;
    private Button buttonRoutes;
    private Button buttonStatistics;
    private Button buttonSettings;
    private Button buttonDailyGoals;
    private Button buttonLogout;
    private Firebase myFirebaseRef;
    private Context context;




    public HomeFragment() {

    }

    public HomeFragment(Context context) {
        this.context = context;
    }

    HomeFragment(Bundle savedInstanceState) {
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Firebase.setAndroidContext(getContext());
        String uid = getActivity().getIntent().getExtras().getString("user_id");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.home_fragment, container,
                false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    @Override
    public void initUI(View view) {
        Firebase.setAndroidContext(getContext());
        myFirebaseRef = new Firebase("https://trackerappv.firebaseio.com/");
        buttonRequestLocation = (Button) view.findViewById(R.id.buttonRequestLocation);
        buttonRequestLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Location Tracking Started!", Toast.LENGTH_SHORT).show();
                ((MainFragmentActivity) context).configureButton();
            }
        });


        buttonMarkLocation = (Button) view.findViewById(R.id.buttonMarkLocation);
        buttonMarkLocation.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((MainFragmentActivity)getActivity()).launchMarkLocation();
            }

        });

        buttonRoutes = (Button) view.findViewById(R.id.buttonRoutes);
        buttonRoutes.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ((MainFragmentActivity)getActivity()).launchMapRoute();
            }

        });

        buttonStatistics = (Button) view.findViewById(R.id.buttonStatistics);
        buttonStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragmentActivity)getActivity()).launchStatisticsView();
            }
        });

        buttonSettings = (Button) view.findViewById(R.id.buttonSettings);
        buttonSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragmentActivity)getActivity()).launchSettingsView();
            }
        });

        buttonDailyGoals = (Button) view.findViewById(R.id.buttonDailyGoals);
        buttonDailyGoals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragmentActivity)getActivity()).launchDailyGoalsView();
            }
        });

        buttonLogout = (Button) view.findViewById(R.id.buttonLogout);
        buttonLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), MainFragment.class);
                myFirebaseRef.unauth();
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }



}
