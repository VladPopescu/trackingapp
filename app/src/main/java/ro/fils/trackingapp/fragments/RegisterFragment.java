package ro.fils.trackingapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.Map;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.model.User;

/**
 * Created by vladp on 6/18/2016.
 */
public class RegisterFragment extends BaseFragment {
    private Firebase myFirebaseRef;
    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextRePassword;
    private Button buttonRegister;

    public RegisterFragment(){
    }

    RegisterFragment(Bundle savedInstanceState) {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.register_fragment, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);

    }


    @Override
    public void initUI(View view) {
        myFirebaseRef =  new Firebase("https://trackerappv.firebaseio.com/");
        editTextEmail = (EditText) view.findViewById(R.id.editTextRegisterEmail);
        editTextPassword = (EditText) view.findViewById( R.id.editTextRegisterPassword);
        editTextRePassword = (EditText) view.findViewById( R.id.editTextRegisterRePassword);
        buttonRegister = (Button) view.findViewById(R.id.buttonRegister);




        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final User user = new User();
                user.setEmail(editTextEmail.getText().toString());
                user.setPassword(editTextPassword.getText().toString());

                if (editTextPassword.getText().toString().equals(editTextRePassword.getText().toString())) {

                    myFirebaseRef.createUser(
                            user.getEmail(),
                            user.getPassword(),
                            new Firebase.ValueResultHandler<Map<String, Object>>() {
                                @Override
                                public void onSuccess(Map<String, Object> stringObjectMap) {
                                    user.setId(stringObjectMap.get("uid").toString());
                                    user.saveUser();
                                    myFirebaseRef.unauth();
                                    Toast.makeText(getActivity().getApplicationContext(), "Your Account has been Created!", Toast.LENGTH_LONG).show();
                                    Toast.makeText(getActivity().getApplicationContext(), "Please Login With your Email and Password", Toast.LENGTH_LONG).show();
                                    editTextEmail.setText("");
                                    editTextPassword.setText("");
                                    editTextRePassword.setText("");
                                }


                                @Override
                                public void onError(FirebaseError firebaseError) {
                                    Toast.makeText(getActivity().getApplicationContext(), "" + firebaseError.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                    );
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Password Missmatch!", Toast.LENGTH_LONG).show();
                    editTextPassword.setText("");
                    editTextRePassword.setText("");
                }




            }
        });


    }

    protected void setUpUser(){

    }


    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }
}
