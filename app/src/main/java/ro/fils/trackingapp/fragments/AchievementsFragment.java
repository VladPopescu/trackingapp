package ro.fils.trackingapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;

/**
 * Created by vladp on 7/2/2016.
 */
public class AchievementsFragment extends BaseFragment {



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.achievements_fragment, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);

    }

    @Override
    public void initUI(View view) {
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println();
//        System.out.println();
    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }
}
