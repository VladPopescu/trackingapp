package ro.fils.trackingapp.fragments.mainFragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.widget.EditText;
import android.widget.Toast;


import com.firebase.client.Firebase;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import ro.fils.trackingapp.fragments.AchievementsFragment;
import ro.fils.trackingapp.fragments.menuFragments.DailyGoalsFragment;
import ro.fils.trackingapp.fragments.menuFragments.HomeFragment;
import ro.fils.trackingapp.fragments.menuFragments.LeaderboardsFragment;
import ro.fils.trackingapp.fragments.menuFragments.MapRouteFragment;
import ro.fils.trackingapp.fragments.menuFragments.MarkLocationFragment;
import ro.fils.trackingapp.fragments.menuFragments.SettingsFragment;
import ro.fils.trackingapp.fragments.menuFragments.StatisticsFragment;
import ro.fils.trackingapp.fragments.mapFragments.FragmentMap;
import ro.fils.trackingapp.fragments.mapFragments.FragmentMarkMap;
import ro.fils.trackingapp.model.LeaderboardEntry;
import ro.fils.trackingapp.model.LocationAddress;
import ro.fils.trackingapp.model.LocationPoint;
import ro.fils.trackingapp.R;
import ro.fils.trackingapp.utils.MainAppFragment;

/**
 * Created by vladp on 4/24/2016.
 */


public class MainFragmentActivity extends FragmentActivity {

    private LocationManager locationManager;
    private LocationListener locationListener;
    private Firebase messageListRef;
    private HomeFragment fragmentHome;
    private FragmentMarkMap fragmentMarkMap;
    private FragmentMap fragmentMap;
    private MapRouteFragment fragmentMapRoute;
    private MarkLocationFragment fragmentMarkLocation;
    private StatisticsFragment fragmentStatistics;
    private SettingsFragment fragmentSettings;
    private DailyGoalsFragment fragmentDailyGoals;
    private LeaderboardsFragment fragmentLeaderboards;
    private AchievementsFragment fragmentAchievements;
    public MainAppFragment fragmentMainApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String uid = getIntent().getExtras().getString("user_id");
        fragmentMainApp = new MainAppFragment();
        fragmentMainApp.getUserLocations(uid);
        fragmentMainApp.getUserMarkers(uid);
        fragmentMainApp.getUserGoals(uid);
        fragmentMainApp.getLeaderBoards();
        fragmentMainApp.getUserAccountDetails(uid);
        fragmentMainApp.getUserAchievements(uid);

        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        final String date = dateFormatter.format(Calendar.getInstance().getTime());



        messageListRef = new Firebase("https://trackerappv.firebaseio.com/"+ uid +"/locations/");
        launchHome();

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                DateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
                String time = timeFormatter.format(Calendar.getInstance().getTime());
                LocationAddress address = null;
                try {
                    address = getAddressLocation(location.getLatitude(), location.getLongitude());
                } catch (IOException e) {
                    e.printStackTrace();
                }

                LocationPoint locationPoint = new LocationPoint(location.getLatitude(), location.getLongitude(), time, date, address);
                Toast.makeText(getApplicationContext(), "Location Point Added!", Toast.LENGTH_SHORT).show();
                messageListRef.push().setValue(locationPoint);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET
            }, 10);

            return;
        } else {
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    configureButton();
                return;
        }
    }



    public void configureButton() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates("gps", 10000, 0, locationListener);
    }

    public LocationAddress getAddressLocation(double latitude, double longitude) throws IOException {
        Geocoder gc = new Geocoder(getApplicationContext(), Locale.getDefault());

        List<Address> addresses = gc.getFromLocation(latitude, longitude, 1);
        LocationAddress locationAddress = new LocationAddress(addresses.get(0).getAdminArea(), addresses.get(0).getCountryName(), addresses.get(0).getLocality(), addresses.get(0).getSubAdminArea(), addresses.get(0).getSubLocality(), addresses.get(0).getSubThoroughfare(), addresses.get(0).getThoroughfare());

        return locationAddress;

    }

    public DateTime convertStringToDate(String time) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");
        DateTime dateF = formatter.parseDateTime(time);
        return dateF;
    }

    public void launchHome() {
        if (fragmentHome == null) {
            fragmentHome = new HomeFragment(this);
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentHome) {

            if (currentFrag != null)    {
                transaction.remove(currentFrag);
            }

            if (!fragmentHome.isAdded()) {
                transaction.add(R.id.frame_container, fragmentHome);
            } else {
                transaction.show(fragmentHome);
            }

            transaction.addToBackStack(null);
            transaction.commit();

        }
    }



    public void launchMarkView(){
        if (fragmentMarkMap == null) {
            fragmentMarkMap = new FragmentMarkMap();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentMarkMap) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentMarkMap.isAdded()) {
                transaction.add(R.id.frame_container, fragmentMarkMap);
            } else {
                transaction.show(fragmentMarkMap);
            }

            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    public void launchMapView(EditText textViewTimeFrom,EditText textViewTimeTo,EditText textViewDate) throws ParseException {
        if (fragmentMap == null) {
            String timeFrom = textViewTimeFrom.getText().toString();
            String timeTo = textViewTimeTo.getText().toString();
            DateTime dateFrom = convertStringToDate(timeFrom);
            DateTime dateTo = convertStringToDate(timeTo);
            fragmentMap = new FragmentMap(textViewDate.getText().toString(), dateFrom, dateTo);
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentMap) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentMap.isAdded()) {
                transaction.add(R.id.frame_container, fragmentMap);
            } else {
                transaction.show(fragmentMap);
            }

            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    public void launchMapRoute() {
        if (fragmentMapRoute == null) {
            fragmentMapRoute = new MapRouteFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentMapRoute) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentMapRoute.isAdded()) {
                transaction.add(R.id.frame_container, fragmentMapRoute);
            } else {
                transaction.show(fragmentMapRoute);
            }

            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    public void launchMarkLocation(){
        if (fragmentMarkLocation == null) {
            fragmentMarkLocation = new MarkLocationFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentMarkLocation) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentMarkLocation.isAdded()) {
                transaction.add(R.id.frame_container, fragmentMarkLocation);
            } else {
                transaction.show(fragmentMarkLocation);
            }

            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    public void launchStatisticsView(){
        if (fragmentStatistics == null) {
            fragmentStatistics = new StatisticsFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentStatistics) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentStatistics.isAdded()) {
                transaction.add(R.id.frame_container, fragmentStatistics);
            } else {
                transaction.show(fragmentStatistics);
            }

            transaction.addToBackStack(null);
            transaction.commit();

        }
    }

    public void launchSettingsView(){
        if (fragmentSettings == null) {
            fragmentSettings = new SettingsFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentSettings) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentSettings.isAdded()) {
                transaction.add(R.id.frame_container, fragmentSettings);
            } else {
                transaction.show(fragmentSettings);
            }

            transaction.addToBackStack(null);
            transaction.commit();
        }
    }


    public void launchDailyGoalsView(){
        if (fragmentDailyGoals == null) {
            fragmentDailyGoals = new DailyGoalsFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentDailyGoals) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentDailyGoals.isAdded()) {
                transaction.add(R.id.frame_container, fragmentDailyGoals);
            } else {
                transaction.show(fragmentDailyGoals);
            }

            transaction.addToBackStack(null);
            transaction.commit();
        }
    }


    public void launchLeaderboardsView(){
        if (fragmentLeaderboards == null) {
            fragmentLeaderboards = new LeaderboardsFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentLeaderboards) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentLeaderboards.isAdded()) {
                transaction.add(R.id.frame_container, fragmentLeaderboards);
            } else {
                transaction.show(fragmentLeaderboards);
            }

            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public void launchAchievementsView(){
        if (fragmentAchievements == null) {
            fragmentAchievements = new AchievementsFragment();
        }
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        android.support.v4.app.Fragment currentFrag = getSupportFragmentManager()
                .findFragmentById(R.id.frame_container);
        if (currentFrag != fragmentAchievements) {

            if (currentFrag != null) {
                transaction.remove(currentFrag);
            }

            if (!fragmentAchievements.isAdded()) {
                transaction.add(R.id.frame_container, fragmentAchievements);
            } else {
                transaction.show(fragmentAchievements);
            }

            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

}
