package ro.fils.trackingapp.fragments.menuFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.firebase.client.Firebase;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.fragments.mainFragments.MainFragment;

/**
 * Created by vladp on 6/18/2016.
 */
public class HomePageFragment extends BaseFragment {
    private Button buttonLoginPage;
    private Button buttonRegisterPage;


    public HomePageFragment() {
    }

    HomePageFragment(Bundle savedInstanceState) {
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Firebase.setAndroidContext(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.home_page_fragment, container,
                false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    @Override
    public void initUI(View view) {

        buttonLoginPage = (Button) view.findViewById(R.id.buttonLoginPage);
        buttonRegisterPage = (Button) view.findViewById(R.id.buttonRegisterPage);

        buttonLoginPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragment)getActivity()).launchLoginView();
            }
        });

        buttonRegisterPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragment)getActivity()).launchRegisterView();
            }
        });






    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }
}
