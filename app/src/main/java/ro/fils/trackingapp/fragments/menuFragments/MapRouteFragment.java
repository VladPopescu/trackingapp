package ro.fils.trackingapp.fragments.menuFragments;

import android.content.Context;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.fragments.mainFragments.MainFragmentActivity;

/**
 * Created by vladp on 4/24/2016.
 */
public class MapRouteFragment extends BaseFragment implements TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    EditText textViewDate;
    EditText textViewTimeFrom;
    EditText textViewTimeTo;

    Button buttonDrawRoute;
    Integer state = 0;
    private Context context;


    public MapRouteFragment() {

    }

    MapRouteFragment(Bundle savedInstanceState) {

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        context = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.route_fragment, container,
                false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }


    @Override
    public void initUI(View view) {
        String uid = getActivity().getIntent().getExtras().getString("user_id");
        System.out.println("In Route Fragment: " + uid);



        textViewDate = (EditText) view.findViewById(R.id.editTextDate);
        textViewTimeFrom = (EditText) view.findViewById(R.id.editTextTimeFrom);
        textViewTimeTo = (EditText) view.findViewById(R.id.editTextTimeTo);


        buttonDrawRoute = (Button) view.findViewById(R.id.buttonDrawRoute);
        buttonDrawRoute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {;
                try {
                    ((MainFragmentActivity)getActivity()).launchMapView(textViewTimeFrom,textViewTimeTo,textViewDate );
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        });


    }

    @Override
    protected void onAfterStart() {
    }

    @Override
    public void onResponse(BaseModel model) {

    }


    public void onStart() {
        super.onStart();
        textViewDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Calendar now = Calendar.getInstance();
                    DatePickerDialog dpd = DatePickerDialog.newInstance(
                            MapRouteFragment.this,
                            now.get(Calendar.YEAR),
                            now.get(Calendar.MONTH),
                            now.get(Calendar.DAY_OF_MONTH)
                    );

                    dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");

                }
                textViewDate.clearFocus();

            }
        });


        textViewTimeFrom.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            MapRouteFragment.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );
                    tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");
                    state = 1;
                }

            }

        });

        textViewTimeTo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    Calendar now = Calendar.getInstance();
                    TimePickerDialog tpd = TimePickerDialog.newInstance(
                            MapRouteFragment.this,
                            now.get(Calendar.HOUR_OF_DAY),
                            now.get(Calendar.MINUTE),
                            false
                    );
                    tpd.show(getActivity().getFragmentManager(), "Timepickerdialog");

                    state = 2;
                }

            }
        });


    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        String secondString = second < 10 ? "0" + second : "" + second;
        final String time = hourString + ":" + minuteString + ":" + secondString;

        switch (state) {
            case 1: {
                textViewTimeFrom.setText(time);
                state = 0;
                break;
            }
            case 2: {
                textViewTimeTo.setText(time);
                state = 0;
                break;
            }

        }

    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Date dateFormat = new Date(year - 1900, monthOfYear, dayOfMonth);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        String date = formatter.format(dateFormat);
        textViewDate.setText(date);
    }







}
