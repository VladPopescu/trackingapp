package ro.fils.trackingapp.fragments.menuFragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.firebase.client.Firebase;


import java.util.ArrayList;
import java.util.List;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;

import ro.fils.trackingapp.fragments.mainFragments.MainFragmentActivity;
import ro.fils.trackingapp.model.LocationMarked;

/**
 * Created by vladp on 6/9/2016.
 */
public class MarkLocationFragment extends BaseFragment {

    private Button buttonMarkPoint;
    private Button buttonViewMarks;
    private EditText textViewLocationName;
    private EditText textViewLocationDescription;
    private Firebase markedLocationRef;
    private LocationManager locationManagerMarkLocation;
    private Spinner spinnerLocation;


    public MarkLocationFragment() {

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Firebase.setAndroidContext(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.mark_location_fragment, container,
                false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);

    }

    @Override
    public void initUI(View view) {
        String uid = getActivity().getIntent().getExtras().getString("user_id");
        markedLocationRef = new Firebase("https://trackerappv.firebaseio.com/"+ uid +"/markedLocations/");

        textViewLocationName = (EditText) view.findViewById(R.id.editTextLocationName);
        textViewLocationDescription = (EditText) view.findViewById(R.id.editTextLocationDescription);
        spinnerLocation = (Spinner) view.findViewById(R.id.spinnerLocationType);
        List<String> list = new ArrayList<String>();
        list.add("Home");
        list.add("Work");
        list.add("Restaurant");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLocation.setAdapter(dataAdapter);


        buttonMarkPoint = (Button) view.findViewById(R.id.buttonMarkPoint);
        buttonMarkPoint.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               locationManagerMarkLocation = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
               String locationName =  textViewLocationName.getText().toString();
               String locationDescription = textViewLocationDescription.getText().toString();
               if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                   return;
               } else{

               }



               Location location = locationManagerMarkLocation.getLastKnownLocation(LocationManager.GPS_PROVIDER);
               LocationMarked point = new LocationMarked(location.getLatitude(), location.getLongitude(), locationName, locationDescription ,spinnerLocation.getSelectedItem().toString());
               markedLocationRef.push().setValue(point);
               Toast.makeText(getActivity().getApplicationContext(), "Location Marked", Toast.LENGTH_SHORT).show();
           }
       });


        buttonViewMarks = (Button) view.findViewById(R.id.buttonViewMarkings);
        buttonViewMarks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragmentActivity)getActivity()).launchMarkView();
            }
        });

    }


    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }


}
