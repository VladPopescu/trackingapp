package ro.fils.trackingapp.fragments.mapFragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import ro.fils.trackingapp.MainApp;
import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.model.LocationMarked;
import ro.fils.trackingapp.utils.MainAppFragment;


/**
 * Created by vladp on 6/11/2016.
 */
public class FragmentMarkMap extends BaseFragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    private ArrayList<LocationMarked> locationsMarked = new ArrayList<>();
    private static View view;

    public FragmentMarkMap() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        try {
            view = inflater.inflate(R.layout.map_mark_fragment, container, false);
        } catch (InflateException e) {

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        locationsMarked = (ArrayList<LocationMarked>) MainAppFragment.userLocationsMarked;
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.mapMarkers)).getMap();

            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }


    private void setUpMarkers(CameraPosition position) throws ParseException {

        List<MarkerOptions> markers = new ArrayList<>();
        int j = 0;
        for (LocationMarked p : locationsMarked) {
            switch (p.getLocationType()) {
                case "Home":
                    MarkerOptions m = new MarkerOptions()
                            .position(new LatLng(p.getCoordinateLatitude(), p.getCoordinateLongitude()))
                            .title(p.getCoordinateName())
                            .snippet(p.getCoordinateDescription())
                            .visible(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.home));
                    markers.add(m);
                    mMap.addMarker(m);
                    j++;
                    break;
                case "Work":
                    MarkerOptions w = new MarkerOptions()
                            .position(new LatLng(p.getCoordinateLatitude(), p.getCoordinateLongitude()))
                            .title(p.getCoordinateName())
                            .snippet(p.getCoordinateDescription())
                            .visible(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.work));
                    markers.add(w);
                    mMap.addMarker(w);
                    j++;
                    break;
                case "Restaurant":
                    MarkerOptions r = new MarkerOptions()
                            .position(new LatLng(p.getCoordinateLatitude(), p.getCoordinateLongitude()))
                            .title(p.getCoordinateName())
                            .snippet(p.getCoordinateDescription())
                            .visible(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant));
                    markers.add(r);
                    mMap.addMarker(r);
                    j++;
                    break;
            }
        }

    }

    private void setUpMap() {

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            @Override
            public void onCameraChange(CameraPosition position) {
                try {
                    setUpMarkers(position);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

        });

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setBuildingsEnabled(false);
        mMap.setIndoorEnabled(true);

        LatLng startingPoint = new LatLng(locationsMarked.get(0).getCoordinateLatitude(), locationsMarked.get(0).getCoordinateLongitude());

        CameraUpdate upd = CameraUpdateFactory.newLatLngZoom(startingPoint,
                15);
        mMap.moveCamera(upd);
        // setUpMarkers();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setPadding(0, 0, 0, 10);
    }

    @Override
    public void onResume() {
        // mMap = null;
        super.onResume();
        setUpMapIfNeeded();
    }

    public void zoomOnEvent() {
        LatLng startingPoint = new LatLng(locationsMarked.get(0).getCoordinateLatitude(), locationsMarked.get(0).getCoordinateLongitude());

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(startingPoint) // Sets the center of the map
                .zoom(22) // Sets the zoom
                .bearing(304) // Sets the orientation of the camera to east
                .tilt(0) // Sets the tilt of the camera to 30 degrees
                .build(); // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onResponse(BaseModel model) {
        // TODO Auto-generated method stub

    }

    @Override
    public void initUI(View view) {

    }

    @Override
    protected void onAfterStart() {
        // TODO Auto-generated method stub

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        final FragmentManager fragManager = this.getFragmentManager();
        final Fragment fragment = fragManager.findFragmentById(R.id.mapMarkers);
        if (fragment != null) {
            fragManager.beginTransaction().remove(fragment).commit();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onDestroy();
    }
}
