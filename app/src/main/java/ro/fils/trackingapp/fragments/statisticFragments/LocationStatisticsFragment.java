package ro.fils.trackingapp.fragments.statisticFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;

/**
 * Created by vladp on 6/14/2016.
 */
public class LocationStatisticsFragment extends BaseFragment{


    public LocationStatisticsFragment(){

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.location_statistics_fragment, container, false);

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    @Override
    public void initUI(View view) {

    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }
}
