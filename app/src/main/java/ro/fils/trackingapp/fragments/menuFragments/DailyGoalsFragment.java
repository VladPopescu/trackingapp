package ro.fils.trackingapp.fragments.menuFragments;

import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.maps.model.LatLng;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ro.fils.trackingapp.R;
import ro.fils.trackingapp.base.BaseFragment;
import ro.fils.trackingapp.base.BaseModel;
import ro.fils.trackingapp.fragments.mainFragments.MainFragmentActivity;
import ro.fils.trackingapp.model.Achievement;
import ro.fils.trackingapp.model.LeaderboardEntry;
import ro.fils.trackingapp.model.LocationPoint;
import ro.fils.trackingapp.model.User;
import ro.fils.trackingapp.model.modelGoal.DistanceGoal;
import ro.fils.trackingapp.model.modelGoal.MarkerGoal;
import ro.fils.trackingapp.model.modelGoal.TimeGoal;
import ro.fils.trackingapp.utils.MainAppFragment;

/**
 * Created by vladp on 6/29/2016.
 */
public class DailyGoalsFragment extends BaseFragment {

    private Button buttonLeaderboard;
    private Button buttonCheckGoals;
    private Button buttonAchievements;
    public static DistanceGoal userDistanceGoal = new DistanceGoal();
    public static TimeGoal userTimeGoal = new TimeGoal();
    public static MarkerGoal userMarkerGoal = new MarkerGoal();
    public static User userLogged = new User();
    public static LeaderboardEntry leaderboardEntry = new LeaderboardEntry();
    public static String uid;
    private ArrayList<LocationPoint> locationPoints = new ArrayList<>();
    private ArrayList<LocationPoint> userDayLocationPoints = new ArrayList<>();


    private ArrayList<LatLng> filteredPoints = new ArrayList<>();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        Firebase.setAndroidContext(getContext());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.goals_fragment, container, false);


        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);

    }


    @Override
    public void initUI(final View view) {
        final String uid = getActivity().getIntent().getExtras().getString("user_id");
        userDistanceGoal = MainAppFragment.userDistanceGoal;
        userTimeGoal = MainAppFragment.userTimeGoal;
        userMarkerGoal = MainAppFragment.userMarkerGoal;
        userLogged = MainAppFragment.userLogged;
        leaderboardEntry = MainAppFragment.leaderboardEntry;

        buttonLeaderboard = (Button) view.findViewById(R.id.buttonLeaderboards);
        buttonLeaderboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragmentActivity) getActivity()).launchLeaderboardsView();
            }
        });


        TextView TimeGoal = (TextView) view.findViewById(R.id.textViewTimeGoals);
        TimeGoal.setText("Distance: " + (int) userDistanceGoal.getDistanceObjective() + " meters");


        TextView DistanceGoal = (TextView) view.findViewById(R.id.textViewDistanceGoals);
        DistanceGoal.setText("Time: " + (int) userTimeGoal.getTimeObjective() + " minutes");


        TextView MarkerGoal = (TextView) view.findViewById(R.id.textViewMarkerGoals);
        MarkerGoal.setText("Markers: " + (int) userMarkerGoal.getMarkerObjective() + " locations");

        TextView userScore = (TextView) view.findViewById(R.id.textViewScore);
        userScore.setText("SCORE: " + (int) leaderboardEntry.getScore());




        locationPoints = (ArrayList<LocationPoint>) MainAppFragment.userLocationsPoints;
        try {
            userDayLocationPoints = getFilteredUserPoints(locationPoints);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        final double dayDistance = computeDistance(userDayLocationPoints);

        buttonCheckGoals = (Button) view.findViewById(R.id.buttonCheckGoals);
        buttonCheckGoals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkDistanceGoal(dayDistance, uid);
            }
        });

        buttonAchievements = (Button) view.findViewById(R.id.buttonAchievements);
        buttonAchievements.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainFragmentActivity) getActivity()).launchAchievementsView();
            }
        });

        checkGoals(uid);
    }

    @Override
    protected void onAfterStart() {

    }

    @Override
    public void onResponse(BaseModel model) {

    }


    public double computeDistance(ArrayList<LocationPoint> locationPointsFiltered) {
        DecimalFormat df = new DecimalFormat("#.00");
        for (LocationPoint point : locationPointsFiltered) {
            filteredPoints.add(new LatLng(point.getLatitude(), point.getLongitude()));
        }
        double totalDistance = 0.0;
        for (int i = 0; i < filteredPoints.size() - 1; i++) {
            totalDistance = totalDistance + getDistance(filteredPoints.get(i), filteredPoints.get(i + 1));
        }
        totalDistance = Double.valueOf(df.format(totalDistance));
        return totalDistance;
    }

    public int computeTime(ArrayList<LocationPoint> locationPointsFiltered){
        int time = 0;

        for (LocationPoint point : locationPointsFiltered) {

        }

        return time;
    }

    public ArrayList<LocationPoint> getFilteredUserPoints(ArrayList<LocationPoint> locationPoints) throws ParseException {
        ArrayList<LocationPoint> locationPointsFiltered = new ArrayList<>();
        DateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
        final String date = dateFormatter.format(Calendar.getInstance().getTime());

        for (LocationPoint p : locationPoints) {
            if (p.getDateLocation().equals(date)) {
                locationPointsFiltered.add(p);
            }
        }
        return locationPointsFiltered;
    }

    public double getDistance(LatLng LatLng1, LatLng LatLng2) {
        double distance = 0;
        Location locationA = new Location("A");
        locationA.setLatitude(LatLng1.latitude);
        locationA.setLongitude(LatLng1.longitude);
        Location locationB = new Location("B");
        locationB.setLatitude(LatLng2.latitude);
        locationB.setLongitude(LatLng2.longitude);
        distance = locationA.distanceTo(locationB);
        return distance;
    }

    public void checkDistanceGoal(double distance, String uid) {
        if (6000 >= userDistanceGoal.getDistanceObjective()) {
            System.out.println("##### Objective Reached");


            Achievement userAchievement = new Achievement();
            userAchievement.setName(userDistanceGoal.getName());
            userAchievement.setLevel(userDistanceGoal.getLevel());
            userAchievement.setObjective(userDistanceGoal.getDistanceObjective());

            Firebase userAchievementsRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/achievements/");
            userAchievementsRef.push().setValue(userAchievement);


            Firebase distanceGoalRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/goals/distanceGoals/" + userDistanceGoal.getKey());

            Map<String, Object> updateDistanceGoal = new HashMap<String, Object>();

            updateDistanceGoal.put("level", userDistanceGoal.getLevel() + 1);
            updateDistanceGoal.put("name", userDistanceGoal.getName());
            updateDistanceGoal.put("distanceObjective", userDistanceGoal.getDistanceObjective() + userDistanceGoal.getDistanceObjective() /2);
            updateDistanceGoal.put("key", userDistanceGoal.getKey());
            updateDistanceGoal.put("category", userDistanceGoal.getCategory());
            distanceGoalRef.updateChildren(updateDistanceGoal);

            Firebase leaderBoardsRef = new Firebase("https://trackerappv.firebaseio.com/leaderboards/" + uid);
            Map<String, Object> updateLeaderBoard = new HashMap<String, Object>();
            updateLeaderBoard.put("email", userLogged.getEmail());
            updateLeaderBoard.put("score", leaderboardEntry.getScore() + userDistanceGoal.getLevel() * userDistanceGoal.getDistanceObjective());
            leaderBoardsRef.updateChildren(updateLeaderBoard);

            System.out.println("#### Level increased");
        } else {
            System.out.println("##### Objective Not Reached");
        }
    }

    public void checkMarkerGoal(String uid){

    }

    public void checkTimeGoal(String uid){

    }

    public void checkGoals(String uid){

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String tomorrowAsString = dateFormat.format(tomorrow);
        double distanceToday = computeDistance(userDayLocationPoints);

        if(tomorrowAsString.equals(userDistanceGoal.getDateGoal())){
            Toast.makeText(getActivity().getApplicationContext(), "Distance Daily Goal Achieved", Toast.LENGTH_LONG).show();
        } else{


            if (6000 >= userDistanceGoal.getDistanceObjective()) {
                System.out.println("##### Objective Reached");


                Achievement userAchievement = new Achievement();
                userAchievement.setName(userDistanceGoal.getName());
                userAchievement.setLevel(userDistanceGoal.getLevel());
                userAchievement.setObjective(userDistanceGoal.getDistanceObjective());

                Firebase userAchievementsRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/achievements/");
                userAchievementsRef.push().setValue(userAchievement);


                Firebase distanceGoalRef = new Firebase("https://trackerappv.firebaseio.com/" + uid + "/goals/distanceGoals/" + userDistanceGoal.getKey());

                Map<String, Object> updateDistanceGoal = new HashMap<String, Object>();

                updateDistanceGoal.put("level", userDistanceGoal.getLevel() + 1);
                updateDistanceGoal.put("name", userDistanceGoal.getName());
                updateDistanceGoal.put("distanceObjective", userDistanceGoal.getDistanceObjective() + userDistanceGoal.getDistanceObjective() /2);
                updateDistanceGoal.put("key", userDistanceGoal.getKey());
                updateDistanceGoal.put("category", userDistanceGoal.getCategory());
                updateDistanceGoal.put("dateGoal", tomorrowAsString);
                distanceGoalRef.updateChildren(updateDistanceGoal);

                Firebase leaderBoardsRef = new Firebase("https://trackerappv.firebaseio.com/leaderboards/" + uid);
                Map<String, Object> updateLeaderBoard = new HashMap<String, Object>();
                updateLeaderBoard.put("email", userLogged.getEmail());
                updateLeaderBoard.put("score", leaderboardEntry.getScore() + userDistanceGoal.getLevel() * userDistanceGoal.getDistanceObjective());
                leaderBoardsRef.updateChildren(updateLeaderBoard);

                System.out.println("#### Level increased");
            } else {
                System.out.println("##### Objective Not Reached");
            }


        }

    }
}
